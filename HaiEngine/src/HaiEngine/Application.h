#pragma once
#include "Core.h"
namespace HaiEngine {
	class HAI_API Application
	{
	public:
		Application();
		virtual ~Application();
		void Run();
	};

	Application* CreateApplication();
}
