#pragma once

#ifdef HE_PLATFORM_WINDOWS
#ifdef HE_BUILD_DLL
#define HAI_API __declspec(dllexport)
#else
#define HAI_API __declspec(dllimport)
#endif // HE_BUILD_DLL
#else
#error Only support windows!
#endif // HE_PLATFORM_WINDOWS
