#pragma once
#ifdef HE_PLATFORM_WINDOWS

extern HaiEngine::Application* HaiEngine::CreateApplication();

int main(int argc, char** argv) {
	auto engine = HaiEngine::CreateApplication();
	engine->Run();
	delete engine;
}

#endif // HE_PLATFORM_WINDOWS
